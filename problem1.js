const fs = require("fs");
const path = require('path');

function createDictionary(filepath){
    fs.mkdir(filepath, function(err){
        if (err){
            console.error(err)
        }
        else{
            console.log("Created directionary successfully");
        }
    })
}
function writefile(filename){
    fs.writeFile(filename ,"HI",function(err){
        if (err){
            console.error(err);
        }
        else {
            console.log("File created successfully")
        }
    })
}
function deletefile(filepath){
    fs.unlink(filepath, function(err){
        if (err){
            console.error(err);
        }
        else {
            console.log("File deleted successfully")
        }
    })
}
function deleteDictionary(filepath){
    fs.rmdir(filepath, function(err){
        if (err){
            console.error(err)
        }
        else{
            console.log("Delete directionary successfully");
        }
    })
}
function generateFiles(n){
    for (let i = 0; i < n; i++){
        writefile(path.resolve(__dirname,`./random/filename${i}.txt`))
    }
}
function deletefiles(n){
    for (let i = 0; i < n; i++){
        deletefile(path.resolve(__dirname,`./random/filename${i}.txt`))
    }
}




module.exports.writefile = writefile;
module.exports.deletefile = deletefile;
module.exports.createDictionary = createDictionary;
module.exports.generateFiles  = generateFiles;
module.exports.deletefiles = deletefiles;
module.exports.deleteDictionary = deleteDictionary;
