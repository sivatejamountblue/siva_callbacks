const path = require('path');
const test = require('../problem2')

const datapath = path.resolve(__dirname, '../lipsum.txt');
const upperCasePath = path.resolve(__dirname, './upperCase.txt');
const lowerCasePath = path.resolve(__dirname, './lowerCase.txt');
const sortedfilePath = path.resolve(__dirname, './sortedFile.txt');
const fileNamePath = path.resolve(__dirname, './filenames.txt');


test.readFile(datapath, function (data) {
    // Read file completed
    test.writeFile(upperCasePath, data.toUpperCase(), function (path1) {
        // Write file completed
        test.appendFile(fileNamePath, path1 , function (path2) {
            test.readFile(path2, function (data2) {
                test.writeFile(lowerCasePath, JSON.stringify(data2.toLowerCase().split('.')), function (path3) {
                    test.appendFile(fileNamePath, path3 , function (path4) {
                        test.readFile(path4, function (data3) {
                            test.writeFile(sortedfilePath, JSON.stringify(JSON.parse(data3).sort()), function (path5) {
                                test.appendFile(fileNamePath,path5, function (path6){
                                    test.readFile(fileNamePath, function (data4){
                                        test.unlink(data4, function (data5){
                                            test.writeFile(fileNamePath, '', function(){
                                                console.log('completed')
                                            })
                                        })
                                    })
                                })
                            })
                        })
                    })
                })
            })
        })
    })
})
