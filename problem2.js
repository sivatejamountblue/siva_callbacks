const fs = require('fs');

function readFile(readFilePath, callback1) {
    fs.readFile(readFilePath, 'utf-8', function (err, data) {
        if (err) {
            callback1(err)
        } else {
            callback1(data)
        }
    })
}

function writeFile(writeFilePath, data, callback2) {
    fs.writeFile(writeFilePath, data, function (err) {
        if (err) {
            callback2(err)
        } else {
            console.log('Writefile Finished')
            callback2(writeFilePath);
        }
    })
}

function appendFile(path1, path2, callback3) {
    fs.appendFile(path1, path2 + '\n', function (err) {
        if (err) {
            callback3(err)
        } else {
            console.log('appendfile Finished')
            callback3(path2);
        }
    })
}

function unlink(unlinkFilePath, callback4) {
    for (let sentence of unlinkFilePath.split('\n')){
        if (sentence.length > 0){
            fs.unlink(sentence, function (err) {
                if (err) {
                    callback4(err)
                } else {
                    console.log('unlink Finished')
                    
                }
            })
        }
    }
    callback4("completed");
}

module.exports.readFile = readFile;
module.exports.writeFile = writeFile;
module.exports.appendFile = appendFile;
module.exports.unlink = unlink;